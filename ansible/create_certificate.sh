#!/bin/bash

openssl req -x509 -newkey rsa:2048 -keyout secrets/haproxy.key -out secrets/haproxy.crt -days 365 -nodes

cat secrets/haproxy.key secrets/haproxy.crt > secrets/haproxy.pem

openssl req -x509 -newkey rsa:2048 -keyout secrets/nginx.key -out secrets/nginx.crt -days 365 -nodes
