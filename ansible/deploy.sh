#!/bin/bash

##############################################################################################################
# GLOBAL
##############################################################################################################

image_debian=debian_systemd:latest
image_centos=centos_systemd:latest
images="debian centos"

##############################################################################################################
# FUNCTIONS
##############################################################################################################

# Run docker debian container
# --------------------------------------------------------
function dockerRunDebian {

	id_container=$1
	image="debian"

	echo "" 
	docker run -tid \
		--cap-add NET_ADMIN \
		--cap-add SYS_ADMIN \
		--cap-add=NET_BROADCAST \
		--cap-add=NET_RAW \
        --sysctl net.ipv4.ip_nonlocal_bind=1 \
        --sysctl net.ipv4.conf.all.arp_announce=2 \
        --sysctl net.ipv4.conf.all.arp_ignore=1 \
        --sysctl net.ipv4.ip_forward=1 \
		--security-opt apparmor:unconfined \
		--stop-signal=SIGRTMIN+3 \
		--publish-all=true \
		--mount type=tmpfs,destination=/run,tmpfs-mode=1750 \
		--mount type=tmpfs,destination=/run/lock,tmpfs-mode=1750 \
		--mount type=tmpfs,destination=/run/sshd,tmpfs-mode=1750 \
		-v /sys/fs/cgroup:/sys/fs/cgroup:ro \
		--name ${USER}-${image}-${id_container} \
		--label owner=${USER} \
		--label distrib=${image} \
		-h ${USER}-${image}-${id_container} \
		${image_debian}
	docker exec -ti ${USER}-${image}-${id_container} /bin/sh -c "useradd -m ${USER}"
	docker exec -ti ${USER}-${image}-${id_container} /bin/sh -c "mkdir ${HOME}/.ssh && chmod 700 ${HOME}/.ssh && chown ${USER}:${USER} $HOME/.ssh"
	docker cp $HOME/.ssh/id_rsa.pub ${USER}-${image}-${id_container}:$HOME/.ssh/authorized_keys
	docker exec -ti ${USER}-${image}-${id_container} /bin/sh -c "chmod 600 ${HOME}/.ssh/authorized_keys && chown ${USER}:${USER} $HOME/.ssh/authorized_keys"
	docker exec -ti ${USER}-${image}-${id_container} /bin/sh -c "echo '${USER}   ALL=(ALL) NOPASSWD: ALL'>>/etc/sudoers"
	containerHostname=$(docker exec -ti ${USER}-${image}-${id_container} /bin/sh -c "hostname")
	containerIP=$(docker inspect -f '{{.NetworkSettings.IPAddress }}' ${USER}-${image}-${id_container})
	echo "------------------------"
	echo "hostname : ${containerHostname}"
	echo "IP       : ${containerIP}"
	echo "------------------------"

}

# Run docker centOS container
# --------------------------------------------------------
function dockerRunCentOS {

	id_container=$1
	image="centos"

	echo "" 
	docker run -tid \
		--cap-add NET_ADMIN \
		--cap-add SYS_ADMIN \
		--cap-add=NET_BROADCAST \
		--cap-add=NET_RAW \
		--security-opt apparmor:unconfined \
		--stop-signal=SIGRTMIN+3 \
		--publish-all=true \
		--mount type=tmpfs,destination=/run,tmpfs-mode=1750 \
		--mount type=tmpfs,destination=/run/lock,tmpfs-mode=1750 \
		--mount type=tmpfs,destination=/run/sshd,tmpfs-mode=1750 \
		-v /sys/fs/cgroup:/sys/fs/cgroup:ro \
		--name ${USER}-${image}-${id_container} \
		--label owner=${USER} \
		--label distrib=${image} \
		-h ${USER}-${image}-${id_container} \
		${image_centos}
	docker exec -ti ${USER}-${image}-${id_container} /bin/sh -c "useradd -m ${USER}"
	docker exec -ti ${USER}-${image}-${id_container} /bin/sh -c "mkdir ${HOME}/.ssh && chmod 700 ${HOME}/.ssh && chown ${USER}:${USER} $HOME/.ssh"
	docker cp $HOME/.ssh/id_rsa.pub ${USER}-${image}-${id_container}:$HOME/.ssh/authorized_keys
	docker exec -ti ${USER}-${image}-${id_container} /bin/sh -c "chmod 600 ${HOME}/.ssh/authorized_keys && chown ${USER}:${USER} $HOME/.ssh/authorized_keys"
	docker exec -ti ${USER}-${image}-${id_container} /bin/sh -c "echo '${USER}   ALL=(ALL) NOPASSWD: ALL'>>/etc/sudoers"
	docker exec -ti ${USER}-${image}-${id_container} /bin/sh -c "echo '${USER}' | passwd --stdin ${USER}"
	containerHostname=$(docker exec -ti ${USER}-${image}-${id_container} /bin/sh -c "hostname")
	containerIP=$(docker inspect -f '{{.NetworkSettings.IPAddress }}' ${USER}-${image}-${id_container})
	echo "------------------------"
	echo "hostname : ${containerHostname}"
	echo "IP       : ${containerIP}"
	echo "------------------------"

}

##############################################################################################################
# MAIN
##############################################################################################################

# Build docker debian image
# --------------------------------------------------------
if [ "$1" == "--build-debian" ] || [ "$1" == "-bd" ]; then

	docker build -t ${image_debian} ./docker/debian

# Build docker centOS image
# --------------------------------------------------------
elif [ "$1" == "--build-centos" ] || [ "$1" == "-bc" ]; then

	docker build -t ${image_centos} ./docker/centos

# Run docker debian container
# --------------------------------------------------------
elif [ "$1" == "--create-debian" ] || [ "$1" == "-cd" ]; then
	
	image="debian"

	# Get number of container parameter
	nb_container=1
	if [ "$2" != "" ]; then
		nb_container=$2
	fi
  
	# Default container range
	min_container=1
	max_container=0

	# Get max id
	idmax_container=`docker ps -a -f label=owner=${USER} -f label=distrib=${image} --format "{{.Names}}" | awk -F "-" '{print $3}' | sort -r | head -1`

	# Override range container values
	min_container=$((${idmax_container} + 1))
	max_container=$((${idmax_container} + ${nb_container}))

	# Launch container
	for id_container in $(seq ${min_container} ${max_container}); do
		dockerRunDebian ${id_container}
	done

# Run docker centOS container
# --------------------------------------------------------
elif [ "$1" == "--create-centos" ] || [ "$1" == "-cc" ]; then
	
	image="centos"

	# Get number of container parameter
	nb_container=1
	if [ "$2" != "" ]; then
		nb_container=$2
	fi
  
	# Default container range
	min_container=1
	max_container=0

	# Get max id
	idmax_container=`docker ps -a -f label=owner=${USER} -f label=distrib=${image} --format "{{.Names}}" | awk -F "-" '{print $3}' | sort -r | head -1`

	# Override range container values
	min_container=$((${idmax_container} + 1))
	max_container=$((${idmax_container} + ${nb_container}))

	# Launch container
	for id_container in $(seq ${min_container} ${max_container}); do
		dockerRunCentOS ${id_container}
	done

# Drop container
# --------------------------------------------------------
elif [ "$1" == "--remove" ] || [ "$1" == "-r" ]; then

	echo ""
	echo "Removing containers..."
	for container in $(docker ps -a -f label=owner=${USER} --format "{{.Names}}");do      
		echo ""
		containerIP=$(docker inspect -f '{{.NetworkSettings.IPAddress }}' ${container})
		echo "Removing of ${container}"
		docker rm -f ${container}
		ssh-keygen -f "/home/${USER}/.ssh/known_hosts" -R "${containerIP}"
	done
	echo ""
	echo "End of remove"

# Startup existing container
# --------------------------------------------------------
elif [ "$1" == "--start" ] || [ "$1" == "-s" ]; then
  
	echo "Starting containers..."
	docker start $(docker ps -a -f label=owner=${USER} -f status=exited --format "{{.Names}}")
	echo "End of start process"

# Stop Existing container
# --------------------------------------------------------
elif [ "$1" == "--stop" ] || [ "$1" == "-p" ]; then
  
	echo "Stopping containers..."
	docker stop $(docker ps -a -f label=owner=${USER} -f status=running --format "{{.Names}}")
	echo "End of stop process"

# View informations from container
# --------------------------------------------------------
elif [ "$1" == "--infos" ] || [ "$1" == "-i" ]; then
  
	echo ""
	echo "Informations about containers : "
	echo ""
    echo "------------------------"

	for container in $(docker ps -a -f label=owner=${USER} --format "{{.Names}}");do      
		containerIP=$(docker inspect -f '{{.NetworkSettings.IPAddress }}' ${container})
		echo "container : ${container}"
		echo "IP        : ${containerIP}"
		echo "------------------------"
	done

	echo ""
	echo "all:"
	echo "  hosts:"
	for container in $(docker ps -a -f label=owner=${USER} --format "{{.Names}}");do      
		containerIP=$(docker inspect -f '{{.NetworkSettings.IPAddress }}' ${container})
        echo "    ${container}:"
        echo "      ansible_host: ${containerIP}"
		echo "      ansible_user: ${USER}"
	done

# Launch ansible script
# --------------------------------------------------------
elif [ "$1" == "--ansible" ] || [ "$1" == "-a" ]; then
  
	# définition du dossier ansible
	ansibleDirectory="."
	if [ "$2" != "" ]; then
		ansibleDirectory=$2
	fi

	ansible-playbook ${ansibleDirectory}/playbook.yml

# View help
# --------------------------------------------------------
else

	echo ""
	echo "Parameters :"
	echo "  -bd, --build-debian        Build docker debian image"
	echo "  -bc, --build-centos        Build docker centOS image"
	echo "  -cd, --create-debian <nb>  Launch of <nb> debian containers"
	echo "  -cc, --create-centos <nb>  Launch of <nb> centOS containers"
	echo "  -r, --remove               Removing existing containers"
	echo "  -i, --infos                Informations about containers"
	echo "  -s, --start                Start existing containers"
	echo "  -p, --stop                 Stop existing containers"
	echo "  -a, --ansible <dir>        Launch ansible script (Current folder by default)"
	echo "  -?, --help                 Help"
	echo ""

fi
