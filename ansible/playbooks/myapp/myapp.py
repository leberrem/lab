#!/usr/bin/python
import socket
from flask import Flask, render_template, request
app = Flask(__name__)

@app.route('/check')
def check(path=None):
        return "OK"

@app.route('/<path:path>')
@app.route('/')
def index(path=None):
        proxyIP = request.remote_addr
        forwardIP = request.headers.get('X-Forwarded-For', request.remote_addr)
        hostname = socket.gethostname()
        platform = request.user_agent.platform
        browser = request.user_agent.browser
        return render_template('index.j2',
                                proxyIP=proxyIP, 
                                forwardIP=forwardIP, 
                                hostname=hostname, 
                                platform=platform, 
                                browser=browser,
                                path=path
                               )

if __name__ == '__main__':
        app.run(host='0.0.0.0', port=5000)
