const express = require('express');
const favicon = require('serve-favicon');
const bodyParser = require('body-parser');
const multer = require('multer');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const mongoose = require('mongoose');
const faker = require('faker');
const config = require('./config');

const app = express();
const upload = multer();

const PORT = 3000;

const secret = "YZuIsRAn01HAaEYnIB9qobXt3yuzMaJ_Ngg_POj5apkNUaOE0n3dc";
const fakeUser = { email: 'admin', password: 'admin'};


mongoose.connect(`mongodb+srv://${config.db.user}:${config.db.password}@expressmovie-mmedz.mongodb.net/test?retryWrites=true&w=majority`);
const db = mongoose.connection;
db.on('error', () => {
    console.error.bind(console, 'connection error : cannont connect to my DB');
});
db.once('open', () => {
    console.log('connected to my DB');
});

const movieSchema = mongoose.Schema({
    movieTitle: String,
    movieYear: Number
});

const Movie = mongoose.model('Movie', movieSchema);

// faker.locale = "fr";
// const title = faker.lorem.sentence(2);
// const year = 1950 + faker.random.number(80);

// const myMovie = new Movie({movieTitle: title, movieYear: year});

// myMovie.save((err, savedMovie) => {
//     if (err) {
//         console.error(err);
//     } else {
//         console.log('savedMovie : ', savedMovie);
//     }
// });

// Middleware des pages statiques
app.use('/public', express.static(`${__dirname}/public`));

// Middleware Express JWT
//app.use(expressJwt({ secret: secret}).unless({ path: ['/', '/movies', '/movie-search', '/login']}));
app.use('/private', expressJwt({ secret: secret}));

// Redirect not authenticated
app.use(function(err, req, res, next) {
    if(401 == err.status) {
        res.redirect('/login')
    }
});

// Middleware favicon.ico
app.use(favicon(`${__dirname}/public/favicon.ico`));

// Middleware de parsing du corps de requête
var urlEncodedParser = bodyParser.urlencoded({ extended: false });

app.set('views', './views');
app.set('view engine', 'ejs');

app.get('/movies', (req, res) => {

    const title = 'Films français'

    // frenchMovies = [
    //     {title: 'La soupe aux choux', year: '1981'},
    //     {title: 'Buffet froid', year: '1979'},
    //     {title: 'Le dîner de cons', year: '1998'}
    // ];

    frenchMovies = [];

    Movie.find((err, movies) => {
        if (err) {
            console.error(err);
            res.statusCode(500);
        } else {
            console.log(movies);
            frenchMovies = movies.map(function(movie) {
                return {id: movie._id, title: movie.movieTitle, year: movie.movieYear}
            });
            res.render('movies', { movies: frenchMovies, title: title });
        }
    });

});

app.get('/movie-search', (req, res) => {

    const title = 'Recherche de film'

    res.render('movie-search');
});

app.get('/private', (req, res) => {

    const tokenPayload = req.user;

    res.render('secure', { user: tokenPayload.user });
});

// app.post('/movies', urlEncodedParser, (req, res) => {
//     console.log(req.body);
//
//     newMovie = { title: req.body.movieTitle, year: req.body.movieYear };
//     // frenchMovies.push(newMovie);
//     frenchMovies = [...frenchMovies, newMovie];
//
//     res.sendStatus(201);
// });

app.post('/movies', upload.fields([]), (req, res) => {
    if (!req.body) {
        console.error('body vide!!!')
        res.sendStatus(500);
    } else {
        const formData = req.body;
        console.log('formData: ', formData);
        // const newMovie = { title: formData.movieTitle, year: formData.movieYear };
        // frenchMovies = [...frenchMovies, newMovie];
        const myMovie = new Movie({movieTitle: formData.movieTitle, movieYear: formData.movieYear});

        myMovie.save((err, savedMovie) => {
            if (err) {
                console.error(err);
                res.sendStatus(500);
            } else {
                console.log(savedMovie);
                res.sendStatus(201);
            }
        });

    }
});

app.get('/movies/add', (req, res) => {
    res.send('Ajout du film');
});

app.get('/movies/:id', (req, res) => {
    const id = req.params.id;
    Movie.findById(id, (err, movie) => {
        if (err) {
            res.statusCode(500);
        } else {
            res.render('movie-details', { movieId: movie._id, movieTitle: movie.movieTitle, movieYear: movie.movieYear});
        }
    });
});

app.post('/movies/:id', urlEncodedParser, (req, res) => {
    const id = req.params.id;
    if (!req.body) {
        res.sendStatus(500);
    } else {
        console.log('movieTitle: ', req.body.movieTitle, 'movieYear: ', req.body.movieYear);
        Movie.findByIdAndUpdate(id, {$set: {movieTitle: req.body.movieTitle, movieYear: req.body.movieYear}}, {new: true}, (err, movie) => {
            if (err) {
                console.log(err);
                res.send(500, `POST Request the movie id ${id} could not be updated`);

            } else {
                console.log(movie);
                // res.send(201, `PUT Request the movie id ${id} updated successfully`);
                res.redirect('/movies');
            }
        })
    }

});

app.delete('/movies/:id', (req, res) => {
    const id = req.params.id;
    Movie.findByIdAndRemove(id, (err, movie) => {
        if (err) {
            console.log(err);
            res.send(500, `DELETE Request the movie id ${id} could not be deleted`);
        } else {
            res.send(202, `DELETE Request the movie id ${id} deleted successfully`);
        }
    })

});
app.post('/movie-search', upload.fields([]), (req, res) => {
    if (!req.body) {
        res.sendStatus(500);
    } else {
        const formData = req.body;
        console.log('formData: ', formData);
        const newMovie = { title: formData.movieTitle, year: formData.movieYear };
        frenchMovies = [...frenchMovies, newMovie];
        res.sendStatus(201);
    }
});

app.get('/login', (req, res) => {
    res.render('login', { title: 'Espace membre' });
});

app.post('/login', upload.fields([]), (req, res) => {
    if (!req.body) {
        res.sendStatus(500);
    } else {
        const formData = req.body;
        console.log('formData: ', formData);
        const loginData = { email: formData.email, password: formData.password };
        if (loginData.email == fakeUser.email && loginData.password == fakeUser.password) {
            const myToken = jwt.sign({ iss: 'http://expresmovies.fr', user: 'admin', role: 'moderator' }, secret);
            res.json(myToken);
            // res.json({
            //     email: 'admin@test.fr',
            //     favoriteMovie: 'Alien',
            //     lastLoginData: new Date()
            // });
        } else {
            res.sendStatus(401);
        }
        res.sendStatus(201);
    }
});

app.get('/', (req, res) => {
    res.render('index');
});

app.listen(PORT, () => {
    console.log(`listening on port ${PORT}`);
});