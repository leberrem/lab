# Initialisation du master

## Installation initiale

sudo kubeadm init --apiserver-advertise-address=192.168.56.101 --node-name $HOSTNAME --pod-network-cidr=10.244.0.0/16

## Initialisation de kubectl

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

## Configuration réseau

sudo sysctl net.bridge.bridge-nf-call-iptables=1

## Installation de flannel

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/a70459be0084506e4ec919aa1c114638878db11b/Documentation/kube-flannel.yml

## Verification de l'etat du cluster

kubectl get nodes
kubectl get pods --all-namespaces

# Initialisation du node

## Configuration réseau

sudo sysctl net.bridge.bridge-nf-call-iptables=1

## Ajout du node au cluster

sudo kubeadm join 192.168.56.101:6443 --token ovcr24.1zgcmjg2ydb7m7e4 \
    --discovery-token-ca-cert-hash sha256:149efd8bd8424dd8465251b3a4d07141a56890314320d26ad676aac19d83aad5