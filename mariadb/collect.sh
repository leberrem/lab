user=$1
password=$2
collectHost=$3
collectPort=$4

csvFile=/tmp/data.csv
date_jour=$(date +%Y-%m-%d-%H-%M)

echo "### data for cmdb ####" > ${csvFile}

############################# Base de données ######################################

# taille des bases
MYSQL_PWD=${password} mysql -u ${user} -sN -e "
SELECT table_schema 'Data Base Name',
       round(sum( data_length + index_length) / 1024 / 1024, 2) 'Data Base Size in MB'
FROM information_schema.TABLES
WHERE table_schema not in ('mysql', 'performance_schema', 'information_schema')
GROUP BY table_schema;
" | tr "\t" ";" | awk -v date_jour="${date_jour}" -v host="${HOSTNAME}" '{print date_jour";"host";db_taille;"$0}' >> ${csvFile}

# nombre de tables
MYSQL_PWD=${password} mysql -u ${user} -sN -e "
SELECT table_schema,
       count(*)
FROM information_schema.TABLES
WHERE table_schema not in ('mysql', 'performance_schema', 'information_schema')
GROUP BY table_schema;
" | tr "\t" ";" |  awk -v date_jour="${date_jour}" -v host="${HOSTNAME}" '{print date_jour";"host";db_tables;"$0}' >> ${csvFile}

# nombre lignes par table
MYSQL_PWD=${password} mysql -u ${user} -sN -e "
SELECT table_schema,
       table_name,
       TABLE_ROWS
FROM information_schema.TABLES
WHERE table_schema not in ('mysql', 'performance_schema', 'information_schema')
GROUP BY table_schema,table_name;
"| tr "\t" ";" |  awk -v date_jour="${date_jour}" -v host="${HOSTNAME}" '{print date_jour";"host";db_lines;"$0}' >> ${csvFile}

# Volume physique des bases
for database in $(MYSQL_PWD=${password} mysql -u ${user} -sN -e "select table_schema from information_schema.TABLES where table_schema not in ('mysql', 'performance_schema', 'information_schema') group by TABLE_SCHEMA;");do
       sudo du -xksh /var/lib/mysql/* | grep ${database} | awk -v date_jour="${date_jour}" -v host="${HOSTNAME}" -v database="$database" '{print date_jour";"host";db_vol_physique;"database";"$1";"$2}' >> ${csvFile}
done

############################# Système ################################################

# volume de la machine en Mo
volume=$(df -Plm | grep -v 'Filesystem' | awk '{sum += $2} END {print sum/1000}' 2> /dev/null)

# system OS
system=$(lsb_release -ds 2>/dev/null)

# mémoire
memoire=$(cat /proc/meminfo | grep MemTotal | awk '{printf("%.1f", $2/1024/1024);}')

echo "${volume};${system};${memoire}" | awk -v date_jour="${date_jour}" -v host="${HOSTNAME}" '{print date_jour";"host";system;"$0}' >> ${csvFile}

############################# updload ################################################

curl -i -X PUT ${collectHost}:${collectPort}/datas?machine=${HOSTNAME}\&type=mysql --upload-file ${csvFile}
