#!/bin/bash

hosts=$(sed -n '/mariadb/,/central/p' hosts.yml | grep -oP "172\.\d+\.\d+.\d+")
user=leberrem
password=funboard
collectHost=$(sed -n '/central/,$p' hosts.yml | grep -oP "172\.\d+\.\d+.\d+")
collectPort=5000

for host in $hosts; do
    echo "Hosts : $host"
    ssh ${USER}@${host} 'bash -s' < collect.sh ${user} ${password} ${collectHost} ${collectPort}
done
