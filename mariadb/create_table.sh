#!/bin/bash

# création tables

hosts=$(sed -n '/mariadb/,/central/p' hosts.yml | grep -oP "172\.\d+\.\d+.\d+")
user=leberrem
password=funboard
databases="mlb1 mlb2 mlb3"

echo ""
echo "Début de création des tables..."
echo ""

for host in $hosts; do
    echo "Host ${host}..."
	for database in $databases; do
        echo "Databse ${database}..."
  	    for tableNum in $(seq 1 10); do
			MYSQL_PWD=${password} mysql -h ${host} -u ${user} -e "
            create table table${tableNum} (champs1 int, 
                                           champs2 varchar(255),
                                           champs3 varchar(255),
                                           champs4 varchar(255),
                                           champs5 varchar(255),
                                           champs6 varchar(255),
                                           champs7 varchar(255),
                                           champs8 varchar(255),
                                           champs9 varchar(255),
                                           champs10 varchar(255),
                                           champs11 varchar(255),
                                           champs12 varchar(255),
                                           champs13 varchar(255),
                                           champs14 varchar(255));
            " ${database}
		done
		echo "Database ${database} == OK"
	done
	echo "Host ${host} => OK"
done

echo ""
echo "Fin de création des tables..."
echo ""
