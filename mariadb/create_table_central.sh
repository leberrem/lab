#!/bin/bash

hosts=$(sed -n '/central/,$p' hosts.yml | grep -oP "172\.\d+\.\d+.\d+")
user=leberrem
password=funboard
databases="collect"

echo ""
echo "Début de création des tables..."
echo ""

for host in $hosts; do
    echo "Host ${host}..."
	for database in $databases; do
        echo "Databse ${database}..."

        MYSQL_PWD=${password} mysql -h ${host} -u ${user} -e "CREATE TABLE central_db_taille (
        taille_date varchar(20),
        taille_machine varchar(50),
        taille_base varchar(20),
        taille_volume_mb float(10)
        );
        " ${database}

        MYSQL_PWD=${password} mysql -h ${host} -u ${user} -e "CREATE TABLE central_db_lines(
        lines_date varchar(20),
        lines_machine varchar(50),
        lines_base varchar(20),
        lines_table varchar(20),
        lines_nb_lines int(10)
        );
        " ${database}

        MYSQL_PWD=${password} mysql -h ${host} -u ${user} -e "CREATE TABLE central_db_volumesphy(
        volumesphy_date varchar(50),
        volumephy_machine varchar(50),
        volumephy_base varchar(20),
        volumephy_taille float(10),
        volumephy_nb_lines varchar(50)
        );
        " ${database}

        MYSQL_PWD=${password} mysql -h ${host} -u ${user} -e "CREATE TABLE central_system(
        system_date varchar(50),
        system_machine varchar(50),
        system_volume float(10),
        system_os varchar(50),
        system_memory_mb float(10)
        );
        " ${database}

		echo "Database ${database} == OK"
	done
	echo "Host ${host} => OK"
done

echo ""
echo "Fin de création des tables..."
echo ""
