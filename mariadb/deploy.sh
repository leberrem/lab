#!/bin/bash

image_name=debian_systemd
image_version=latest

# -------------------------------------
# Construction de l'image
# -------------------------------------
if [ "$1" == "--build" ] || [ "$1" == "-b" ]; then

	docker build -t ${image_name}:${image_version} .

# -------------------------------------
# Création des conteneurs
# -------------------------------------
elif [ "$1" == "--create" ] || [ "$1" == "-c" ]; then
	
	# définition du nombre de conteneur
	nb_conteneur=1
	[ "$2" != "" ] && nb_conteneur=$2
  
	# valeur par défaut
	min_conteneur=1
	max_conteneur=0

	# récupération de l'ID maximum
	idmax_conteneur=`docker ps -a --format '{{ .Names}}' | awk -F "-" -v user="${USER}" -v image_name="${image_name}" '$0 ~ user"-"image_name {print $3}' | sort -r | head -1`

	# redéfinition de min et max
	min_conteneur=$((${idmax_conteneur} + 1))
	max_conteneur=$((${idmax_conteneur} + ${nb_conteneur}))

	# lancement des conteneurs
	for i in $(seq ${min_conteneur} ${max_conteneur}); do
		docker run -tid --cap-add NET_ADMIN --cap-add SYS_ADMIN --publish-all=true --tmpfs /run --tmpfs /run/lock -v /sys/fs/cgroup:/sys/fs/cgroup:ro --name ${USER}-${image_name}-$i -h ${USER}-${image_name}-$i ${image_name}:${image_version}
		docker exec -ti ${USER}-${image_name}-$i /bin/sh -c "useradd -m ${USER}"
		docker exec -ti ${USER}-${image_name}-$i /bin/sh -c "mkdir  ${HOME}/.ssh && chmod 700 ${HOME}/.ssh && chown ${USER}:${USER} $HOME/.ssh"
    	docker cp $HOME/.ssh/id_rsa.pub ${USER}-${image_name}-$i:$HOME/.ssh/authorized_keys
    	docker exec -ti ${USER}-${image_name}-$i /bin/sh -c "chmod 600 ${HOME}/.ssh/authorized_keys && chown ${USER}:${USER} $HOME/.ssh/authorized_keys"
		docker exec -ti ${USER}-${image_name}-$i /bin/sh -c "echo '${USER}   ALL=(ALL) NOPASSWD: ALL'>>/etc/sudoers"
		docker exec -ti ${USER}-${image_name}-$i /bin/sh -c "mkdir -p /run/sshd"
		docker exec -ti ${USER}-${image_name}-$i /bin/sh -c "systemctl start ssh"
		docker exec -ti ${USER}-${image_name}-$i /bin/sh -c "systemctl enable ssh"
		echo "Conteneur ${USER}-${image_name}-$i créé"
	done

# -------------------------------------
# Supression des conteneurs
# -------------------------------------
elif [ "$1" == "--drop" ] || [ "$1" == "-d" ]; then

	echo "Suppression des conteneurs..."
	for conteneur in $(docker ps -a | grep ${USER}-${image_name} | awk '{print $1}');do      
		docker rm -f $conteneur
	done
	echo "Fin de la suppression"

# -------------------------------------
# Démarrage des conteneurs
# -------------------------------------
elif [ "$1" == "--start" ] || [ "$1" == "-s" ]; then
  
	echo "Démarrage des conteneurs..."
	docker start $(docker ps -a | grep ${USER}-${image_name} | awk '{print $1}')
	echo "Fin de démarrage"

# -------------------------------------
# Arrêt des conteneurs
# -------------------------------------
elif [ "$1" == "--stop" ] || [ "$1" == "-p" ]; then
  
	echo "Arrêt des conteneurs..."
	docker stop $(docker ps -a | grep ${USER}-${image_name} | awk '{print $1}')
	echo "Fin de l'arrêt"

# -------------------------------------
# Information sur les conteneurs
# -------------------------------------
elif [ "$1" == "--infos" ] || [ "$1" == "-i" ]; then
  
	echo ""
	echo "Informations sur les conteneurs : "
	echo ""

	for conteneur in $(docker ps -a | grep ${USER}-${image_name} | awk '{print $1}');do      
		docker inspect -f '   => {{.Name}} - {{.NetworkSettings.IPAddress }}' $conteneur
	done

	echo ""

# -------------------------------------
# Lancement du script ansible
# -------------------------------------
elif [ "$1" == "--ansible" ] || [ "$1" == "-a" ]; then
  
	ansible-playbook -i hosts.yml -b -u ${USER} playbook.yml

# -------------------------------------
# Lancement du service metabase
# -------------------------------------
elif [ "$1" == "--metabase" ] || [ "$1" == "-m" ]; then

	if [ $(docker ps -a | grep -c metabase) == 1 ]; then
		docker start metabase
	else
		echo "docker run -d -p 3000:3000 -v ${PWD}/metabase:/metabase-data -e \"MB_DB_FILE=/metabase-data/metabase.db\" --name metabase metabase/metabase"
		docker run -d -p 3000:3000 -v ${PWD}/metabase:/metabase-data -e "MB_DB_FILE=/metabase-data/metabase.db" --name metabase metabase/metabase
	fi

# -------------------------------------
# Affichage de l'aide
# -------------------------------------
else

	echo ""
	echo "Options :"
	echo "  -b, --build           Construction de l'image docker"
	echo "  -c, --create <nb>     Lancement de <nb> conteneurs"
	echo "  -d, --drop            Suppression les conteneurs créés"
	echo "  -i, --infos           Caractéristiques des conteneurs (ip, nom, user...)"
	echo "  -s, --start           Redémarrage des conteneurs"
	echo "  -p, --stop            Arrêt des conteneurs"
	echo "  -a, --ansible         Déploiement arborescence ansible"
	echo "  -m, --metabase        Lancement de metabase"
	echo ""

fi
