#!/bin/bash

# Suppression des données

hosts=$(sed -n '/mariadb/,/central/p' hosts.yml | grep -oP "172\.\d+\.\d+.\d+")
user=leberrem
password=funboard
databases="mlb1 mlb2 mlb3"

echo ""
for host in $hosts;do
	echo "Host ${host}..."
	for database in ${databases};do
        echo "Databse ${database}..."
      	for tableNum in $(seq 1 10);do
			MYSQL_PWD=${password} mysql -h ${host} -u ${user} -e "
            TRUNCATE table${tableNum};
            " ${database}
		done
		echo "Database ${database} == OK"
	done
    echo "Host ${host} => OK"
done
echo ""
