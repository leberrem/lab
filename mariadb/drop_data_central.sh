#!/bin/bash

# Suppression des données

hosts=$(sed -n '/central/,$p' hosts.yml | grep -oP "172\.\d+\.\d+.\d+")
user=leberrem
password=funboard
databases="collect"
tables="central_db_taille central_db_lines central_db_volumesphy central_system"

echo ""
for host in $hosts;do
	echo "Host ${host}..."
	for database in ${databases};do
        echo "Databse ${database}..."
      	for table in ${tables};do
			MYSQL_PWD=${password} mysql -h ${host} -u ${user} -e "
            TRUNCATE ${table};
            " ${database}
		done
		echo "Database ${database} == OK"
	done
    echo "Host ${host} => OK"
done
echo ""
