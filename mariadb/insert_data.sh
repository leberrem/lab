#!/bin/bash

# insertion de données

hosts=$(sed -n '/mariadb/,/central/p' hosts.yml | grep -oP "172\.\d+\.\d+.\d+")
user=leberrem
password=funboard
databases="mlb1 mlb2 mlb3"
randomValue=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 255 | head -n 1)

echo ""
echo "Début d'insertion des données..."
echo ""

for host in $hosts; do
    echo "Host ${host}..."
    for database in $databases; do
        echo "Databse ${database}..."
        for tableNum in $(seq 1 10); do
            alea=`shuf -i1-500 -n1`
            echo "  table${tableNum} - ${alea} lines..."
            for numLine in $(seq 1 ${alea}); do
                MYSQL_PWD=${password} mysql -h ${host} -u ${user} -e "
                INSERT INTO table${tableNum} 
                VALUES (${numLine}, 
                        '${randomValue}',
                        '${randomValue}',
                        '${randomValue}',
                        '${randomValue}',
                        '${randomValue}',
                        '${randomValue}',
                        '${randomValue}',
                        '${randomValue}',
                        '${randomValue}',
                        '${randomValue}',
                        '${randomValue}',
                        '${randomValue}',
                        '${randomValue}'
                       );
                " ${database}
            done
        done
        echo "Database ${database} == OK"
    done
    echo "Host ${host} => OK"
done

echo ""
echo "Fin d'insertion des données..."
echo ""