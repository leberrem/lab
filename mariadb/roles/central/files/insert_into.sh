## Traitement des lignes des tailles des bases de données :

user=leberrem
password=funboard
CSVFile=$1
SQLFile=${CSVFile%%.*}.sql

> ${SQLFile}

## Traitement des données mysql:

cat $CSVFile | awk -F ";" ' /;db_taille;/ {print "INSERT INTO central_db_taille VALUES ('\''"$1"'\'', '\''"$2"'\'', '\''"$4"'\'', '\''"$5"'\'');"}' >> ${SQLFile}

cat $CSVFile | awk -F ";" ' /;db_lines;/ {print "INSERT INTO central_db_lines VALUES ('\''"$1"'\'', '\''"$2"'\'', '\''"$4"'\'', '\''"$5"'\'', '\''"$6"'\'');"}' >> ${SQLFile}

## Traitement des volumes physiques :

cat $CSVFile | awk -F ";" ' /;db_vol_physique;/ {print "INSERT INTO central_db_volumesphy VALUES ('\''"$1"'\'', '\''"$2"'\'', '\''"$4"'\'', '\''"$5"'\'', '\''"$6"'\'');"}' >> ${SQLFile}

## Traitement des données système:

cat $CSVFile | awk -F ";" ' /;system;/ {print "INSERT INTO central_system VALUES ('\''"$1"'\'', '\''"$2"'\'', '\''"$4"'\'', '\''"$5"'\'', '\''"$6"'\'');"}' >> ${SQLFile}

#insertion en base
MYSQL_PWD=${password} mysql -u ${user} collect < ${SQLFile}
