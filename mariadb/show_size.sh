#!/bin/bash

hosts=$(grep -oP "172\.\d+\.\d+.\d+" hosts.yml)
user=leberrem
password=funboard

for host in $hosts;do
	echo ""
	echo "Host : ${host} "
	MYSQL_PWD=${password} mysql -h ${host} -u ${user} -e "
    SELECT table_schema 'Data Base Name',
           sum( data_length + index_length) / 1024 / 1024 'Data Base Size in MB'
    FROM information_schema.TABLES
    GROUP BY table_schema;
    "
done

echo ""
