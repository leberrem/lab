#!/bin/bash

hosts=$(grep -oP "172\.\d+\.\d+.\d+" hosts.yml)
user=leberrem
password=funboard

echo ""
for host in $hosts;do
  echo "Host : ${host}"
  MYSQL_PWD=${password} mysql -h ${host} -u ${user} -e "show databases;" 
  echo ""
done
