------------------------------------------------------------------------------
# mongod
------------------------------------------------------------------------------

## Démarrage d'une instance personnalisée

mkdir -p /mongo/instance01
chown mongod:mongod /mongo/instance01
sudo su mongod -s /bin/bash -c "/usr/bin/mongod --port 27018 --dbpath /mongo/instance01 --logpath /mongo/instance01/mongod_27018.log --fork"

------------------------------------------------------------------------------
# mongo shell
------------------------------------------------------------------------------

## Arrêt d'un BDD

use admin;
db.shutdownServer();

## Changer le prompt

host = db.serverStatus().host;
prompt = function() { return db+"@"+host+" > "; };

## Aide sur la BDD

db.help();

## Affichage des base de données

show dbs
ou
show databases

## Information système

db.runCommand({buildinfo:1});

## Exemple de création de base de données et collection

use mydb;
db.createCollection('stagiaire');

use mydb;
db.stagiaire.insert({nom: "Doe", prenom: "John", age: 17});

## Affichage des collections

show collections;
ou
db.getCollectionNames();

## Exemple de script sur les collections

db.getCollectionNames().forEach(
    function (collcollection) {
        print(`collection ${collcollection}`);
        printjson(db[collcollection].getIndexes());
    }
);

## Exemple de Chargement fictif d'une collection

use mydb;
for (i=0; i<1000; i++) {db.stagiaire.insert({nom: "Doe_"+i, prenom: "John_"+i})}

## Exemple d'affichage du nombre de documents d'une collection

use mydb;
db.stagiaire.stats().count;

## Exemple d'affichage du nombre de documents d'une collection

use mydb;
db.stagiaire.insert({_id: 123456789, nom: "Doe", prenom: "John"})

db.stagiaire.insertMany([{nom: "Jakson"}, {nom: "Albert"}]);

## Affichage et recherche de documents

use mydb;
db.stagiaire.find().pretty();

db.stagiaire.find({"nom": "Doe"}).pretty();

db.stagiaire.find({"nom": "Doe", "prenom": "John"}).pretty();

db.stagiaire.find({} , {nom:1, prenom:1, _id:0}).pretty();

db.stagiaire.find({}).pretty();

db.stagiaire.findOne();

--------------------------------------------------------------------------------------

db.students.insertMany([{name: "Bob", age: 18, fields: ["musique", "Philosophie"]},
                        {name: "Jeff", age: 24, fields: ["programmation", "réseaux"]},
                        {name: "Greg", age: 19, fields: ["lettres", "programmation"]},
                        {name: "Max", age: 22, fields: ["réseaux", "programmation"]},
                        {name: "Bob", age: 45}
                       ]);
db.students.find({age: {$gt: 19}});
db.students.find({age: {$gte: 19}})
db.students.find({age: {$gt: 19, $lt: 23}});

db.students.find({"name": { "$ne" : "Bob" }});

db.students.find({$or: [{fields: "musique"}, {name: "Max"}]});

db.students.find({$and: [{fields: "programmation"}, {fields: "lettres"}]});

db.students.find({name: {$in: ["Bob", "Greg"]}});

db.students.find({name: {$nin: ["Bob", "Greg"]}});

db.students.find({fields: {$all: ["programmation", "réseaux"]}});

// Existance de champ
db.students.find({fields: {$exists: 1}});
db.students.find({fields: {$exists: 0}});

// Tri de données
db.students.find().sort({name: 1});
db.students.find().sort({age: -1});
db.students.find().sort({name: 1, age: -1});

// Pagination
db.students.find().limit(2);
db.students.find().skip(2).limit(2);
db.students.find().sort({name: 1, age: -1}).skip(3).limit(2);
db.students.find().sort({age: -1}).limit(1);

// Aggregation
db.students.distinct("name");

db.students.aggregate([{ $group: { _id: '$name', count: {$sum: 1} } } ]);

db.students.aggregate([{ $group: { _id: '$name', moyenne: {$avg: '$age'} } } ]);

db.students.aggregate([{ $group: { _id: '$name', moyenne: {$avg: '$age'} } }, {$out: 'ageMoyen'} ]);
db.ageMoyen.find();

// Jointures
----------------------------------------------
use musicshop
db.instruments.insertMany([{name: "Fender Stratocaster"}, {name: "Gibson Les Paul"}, {name: "Fender Jazz Bass"}]);
const guitar_1 = db.instruments.findOne({name: "Fender Stratocaster"});
const guitar_2 = db.instruments.findOne({name: "Gibson Les Paul"});
db.musicians.insertMany([{name: "Jimmy Hendrix", instrumentId: guitar_1._id}, {name: "Jimmy Page", instrumentId: guitar_2._id}]);
const musician_1 = db.musicians.findOne({name: "Jimmy Hendrix"})
db.instruments.findOne({_id: musician_1.instrumentId})
----------------------------------------------
const musicians = db.musicians.find();
while (musicians.hasNext()) {
    const musician = musicians.next();
    const instrument = db.instruments.findOne({_id: musician.instrumentId});
    print(`${musician.name} joue avec une ${instrument.name}`);
};
----------------------------------------------
db.musicians.find().forEach(function(musician) {
    const instrument = db.instruments.findOne({_id: musician.instrumentId});
    print(`${musician.name} joue avec une ${instrument.name}`);
});
----------------------------------------------

## Interpretation de l'ID

db.students.find(ObjectId("5d493ac0c1c9b9002e77444f"))[0]._id.getTimestamp()

http://steveridout.github.io/mongo-object-time/

## recherche avec regexp

db.stagiaire.find({"nom": /^Doe_99/});

db.stagiaire.find({"nom": /^doe_99/i});

db.stagiaire.find({"nom": /^Doe_99/}).sort({nom: -1});

## Exemple de mise à jour de document

db.stagiaire.update({"nom": "Doe"}, {$set:{prenom: "Marc"}});

db.stagiaire.update({"nom": {$regex:'^Doe$', $options:'i'}}, {$set:{prenom: "Marc"}});

db.stagiaire.update({"nom": { "$in" : ["Doe_10","Doe_20"] }}, {$set:{prenom: "Marc"}});

db.stagiaire.update({"nom": "Doe"}, {$set:{ville: "Marc"}});

## Modification document avec Ajout/suppression d'une nouvelle valeur

db.stagiaire.update({"nom": "Doe"}, {$set:{ville: "Nogent"}});
db.stagiaire.find({"nom": "Doe"}).pretty();
db.stagiaire.update({"nom": "Doe"}, {$unset:{ville: "Nogent"}});

## Modification document et incrément

db.stagiaire.update({"nom": "Doe"}, {$set:{num: 10}});
db.stagiaire.find({"nom": "Doe"}).pretty();
db.stagiaire.update({"nom": "Doe"}, {$inc: {num: 5}});

## Modification document et ajout d'un élément dans un tableau

db.stagiaire.update({"nom": "Doe"}, {$push:{fields: "sécurité"}});
db.stagiaire.update({"nom": "Doe"}, {$push:{fields: "informatique"}});

## Modification document et ajout d'un élément dans un tableau avec contrôle de doublon

db.stagiaire.update({"nom": "Doe"}, {$addToSet: {fields: "sécurité"}});

## Modification document et ajout de plusieurs éléments dans un tableau avec contrôle de doublon

db.stagiaire.update({"nom": "Doe"}, {$addToSet: {fields: {$each: ["linux", "unix"]}}});

## Upsert de document

db.stagiaire.update({nom: "Colson"},{nom: "Colson", prenom: "Greg"}, true);

## Exemple de mise à jour complète de document (ou ajout)

db.stagiaire.save({_id: 123456789, nom: "Newman", prenom: "Marc"})

## Exemple de de suppression d'un document

db.stagiaire.remove({_id: 123456789})

## Exemple de suppression d'une collection

db.stagiaire.drop();

## Exemple de suppression d'une base de données

use mydb;
db.runCommand({dropDatabase:1});
ou
db.dropDatabase();

------------------------------------------------------------------------------
# ReplicatSet
------------------------------------------------------------------------------

## Configuration initiale

sudo mkdir -p /mongo/instance01
sudo mkdir -p /mongo/instance02
sudo mkdir -p /mongo/arbitre
sudo chown -R mongodb:mongodb /mongo

sudo su mongodb -s /bin/bash -c "mongod --replSet rs01 --port 27001 --dbpath /mongo/instance01 --logpath /mongo/instance01/mongod.log --fork"
sudo su mongodb -s /bin/bash -c "mongod --replSet rs01 --port 27002 --dbpath /mongo/instance02 --logpath /mongo/instance02/mongod.log --fork"
sudo su mongodb -s /bin/bash -c "mongod --replSet rs01 --port 27004 --dbpath /mongo/arbitre --logpath /mongo/arbitre/mongod.log --fork"

mongo --port 27001 admin
> cfg={_id: "rs01", members:[{_id:1,host:"127.0.0.1:27001"}, {_id:2,host:"127.0.0.1:27002"}, {_id:4,host:"127.0.0.1:27004", arbiterOnly: true}]}
> rs.initiate(cfg);
> db.isMaster();
> rs.status();

mongo --port 27002 admin
> rs.slaveOk();
> db.printReplicationInfo();

## Entrée d'un serveur

sudo mkdir -p /mongo/instance03

sudo su mongodb -s /bin/bash -c "mongod --replSet rs01 --port 27003 --dbpath /mongo/instance03 --logpath /mongo/instance03/mongod.log --fork"

mongo --port 27001 admin
> rs.add({_id:3,host:"127.0.0.1:27003"});

mongo --port 27003 admin
> rs.slaveOk();

## sortie d'un serveur

mongo --port 27003 admin
> rs.remove("127.0.0.1:27001");

## Forcer une synchronisation

rs.syncFrom("127.0.0.1:27003");

------------------------------------------------------------------------------
# Sharding
------------------------------------------------------------------------------

## configure Replicaset for sharding

```shell

sudo mkdir -p /mongo/rs01/instance01
sudo mkdir -p /mongo/rs01/instance02
sudo mkdir -p /mongo/rs01/instance03
sudo mkdir -p /mongo/rs01/arbitre
sudo chown -R mongodb:mongodb /mongo

sudo su mongodb -s /bin/bash -c "mongod --replSet rs01 --shardsvr --port 27001 --dbpath /mongo/rs01/instance01 --logpath /mongo/rs01/instance01/mongod.log --fork"
sudo su mongodb -s /bin/bash -c "mongod --replSet rs01 --shardsvr --port 27002 --dbpath /mongo/rs01/instance02 --logpath /mongo/rs01/instance02/mongod.log --fork"
sudo su mongodb -s /bin/bash -c "mongod --replSet rs01 --shardsvr --port 27003 --dbpath /mongo/rs01/instance03 --logpath /mongo/rs01/instance03/mongod.log --fork"
sudo su mongodb -s /bin/bash -c "mongod --replSet rs01 --shardsvr --port 27004 --dbpath /mongo/rs01/arbitre --logpath /mongo/rs01/arbitre/mongod.log --fork"

mongo --port 27001 admin
> cfg={_id: "rs01", members:[{_id:1,host:"localhost:27001"}, {_id:2,host:"localhost:27002"}, {_id:3,host:"localhost:27003"}, {_id:4,host:"localhost:27004", arbiterOnly: true}]}
> rs.initiate(cfg);
> db.isMaster();
> rs.status();

mongo --port 27002 admin --eval "rs.slaveOk();"
mongo --port 27003 admin --eval "rs.slaveOk();"

```

## configure Replicaset for configuration

```shell

sudo mkdir -p /mongo/rsconfig/configdb_01
sudo mkdir -p /mongo/rsconfig/configdb_02
sudo mkdir -p /mongo/rsconfig/configdb_03
sudo mkdir -p /mongo/rsconfig/configdb_04
sudo chown -R mongodb:mongodb /mongo/rsconfig

sudo su mongodb -s /bin/bash -c "mongod --replSet rsconfig --configsvr --port 27101 --bind_ip_all --dbpath /mongo/rsconfig/configdb_01 --logpath /mongo/rsconfig/configdb_01/mongod.log --fork"
sudo su mongodb -s /bin/bash -c "mongod --replSet rsconfig --configsvr --port 27102 --bind_ip_all --dbpath /mongo/rsconfig/configdb_02 --logpath /mongo/rsconfig/configdb_02/mongod.log --fork"
sudo su mongodb -s /bin/bash -c "mongod --replSet rsconfig --configsvr --port 27103 --bind_ip_all --dbpath /mongo/rsconfig/configdb_03 --logpath /mongo/rsconfig/configdb_03/mongod.log --fork"
sudo su mongodb -s /bin/bash -c "mongod --replSet rsconfig --configsvr --port 27104 --bind_ip_all --dbpath /mongo/rsconfig/configdb_04 --logpath /mongo/rsconfig/configdb_04/mongod.log --fork"

mongo --port 27101 admin
> cfg={_id: "rsconfig", members:[{_id:1,host:"localhost:27101"}, {_id:2,host:"localhost:27102"}, {_id:3,host:"localhost:27103"}, {_id:4,host:"localhost:27104"}]}
> rs.initiate(cfg);
> rs.status();

mongo --port 27102 admin --eval "rs.slaveOk();"
mongo --port 27103 admin --eval "rs.slaveOk();"
mongo --port 27104 admin --eval "rs.slaveOk();"

```

## configure mongos

```shell

sudo mkdir -p /mongo/mongos
sudo chown -R mongodb:mongodb /mongo/mongos

sudo su mongodb -s /bin/bash -c "mongos --port 27201 --configdb rsconfig/localhost:27101,localhost:27102,localhost:27103,localhost:27104 --logpath /mongo/mongos/mongos.log --fork"

mongo --port 27201
mongos> use config
mongos> db.settings.save({ _id:"chunksize", value: 5 })

mongos> use admin
mongos> db.runCommand({addshard:"rs01/localhost:27001,localhost:27002,localhost:27003"});

```

## Test sharding

```shell

mongo --port 27201
mongos> use test
mongos> db.createCollection('clients');
mongos> show collections;
mongos> use admin
mongos> db.runCommand({enablesharding:"test"});
mongos> db.runCommand({shardcollection: "test.clients", key:{"num":1}});
mongos> db.printShardingStatus();

mongos> use test
mongos> for(i=1; i<100000; i++) db.clients.save({num: i, nom: "client_"+i});
mongos> db.clients.dataSize();
mongos> db.clients.totalSize();
mongos> db.clients.stats();

```

------------------------------------------------------------------------------
# Indexation
------------------------------------------------------------------------------

## Création index BTREE simple

db.<collection>.ensureIndex({<champ>:1}); //ASC
db.<collection>.ensureIndex({<champ>:-1}); //DESC

## Création index BTREE composé

db.<collection>.ensureIndex({<champ1>:1, <champ2>:-1});

## Création index BTREE unique

db.<collection>.ensureIndex({<champ>:1}, {unique: true});

## Création index BTREE en arrière plan

db.<collection>.ensureIndex({<champ>:1}, {background: true});

## Visualiser les index d'un collection

db.<collection>.getIndexes();

## Visualiser les index d'une base

db.system.indexes.find();

## Reconstruction d'un index

db.collection.reIndex();

## Suppression d'un index

db.<collection>.dropIndex('<index>');

## Monitoring des opérations sur les index

Progression : db.currentOp();
Interruption : db.killOp();

## Visualiser le plan d'execution

db.<collection>.find().explain();

## Forcer l'utilisation d'un index

db.<collection>.find().hint({<champ>:1});

db.<collection>.find().hint({<champ>:1}).explain();

## test

mongo shell
> use test
> db.createCollection('personnes');
> for(i=1; i<50000; i++) db.personnes.save({num: i, nom: "nom_"+i, prenom: "prenom_"+i, age: i});

watch -d "mongo --port 27201 --eval 'db.personnes.count();' --quiet"

mongo shell
> use test
> db.personnes.find().limit(20);

// Index simple
> db.personnes.createIndex({num:1});

> db.personnes.stats();
> db.personnes.getIndexes();
> db.system.Indexes.find();

> db.personnes.find({age: 10}).explain("executionStats"); # pas d'index utilisé
> db.personnes.find({num: 10}).explain("executionStats"); # index utilisé

> db.personnes.find({nom: "nom_10", prenom: "prenom_10"}).explain("executionStats"); # pas d'index utilisé

// Index unique
> db.personnes.createIndex({nom:1}, {unique: true});

// Index composé (compound index)
> db.personnes.createIndex({nom:1, prenom:1});

> db.personnes.find({nom: "nom_10", prenom: "prenom_10"}).explain("executionStats"); # index utilisé
> db.personnes.find({nom: "nom_10"}).explain("executionStats"); # index utilisé
> db.personnes.find({prenom: "prenom_10"}).explain("executionStats"); # pas d'index utilisé

> db.personnes.find({prenom: "prenom_10"}).hint({nom:1,prenom:1}).explain("executionStats"); # index utilisé mais plus lent

> db.personnes.dropIndex({num:1});

// Index de type texte
> db.personnes.createIndex({nom: "text", prenom: "text"});

> db.personnes.find({$text:{$search:"nom_19"}});

> db.personnes.find({$text:{$search:"nom_19"}}, {nom:1, prenom:1, _id:0, score: {$meta:"textScore"}});

> db.personnes.find({$text:{$search:"nom_19"}}, {nom:1, prenom:1, _id:0, score: {$meta:"textScore"}}).sort({score: {$meta:"textScore"}});

------------------------------------------------------------------------------
# Backup/Restore
------------------------------------------------------------------------------

sudo mkdir -p /mongo/backup
sudo chown -R mongodb:mongodb /mongo/backup

## Sauvegarde d'une collection
sudo su mongodb -s /bin/bash -c "mongodump --port 27001 --collection personnes --db test --out /mongo/backup"

## Sauvegarde d'une base de données
sudo su mongodb -s /bin/bash -c "mongodump --port 27001 --db test --out /mongo/backup"

## Sauvegarde d'une instance
sudo su mongodb -s /bin/bash -c "mongodump --port 27001 --out /mongo/backup"

## Restauration d'une collection
sudo su mongodb -s /bin/bash -c "mongorestore --port 27001 --nsInclude test.clients --drop /mongo/backup"

## Restauration d'une base de données
sudo su mongodb -s /bin/bash -c "mongorestore --port 27001 --db test --drop /mongo/backup/test"

## Restauration d'une instance
sudo su mongodb -s /bin/bash -c "mongorestore --port 27001 --drop /mongo/backup"

------------------------------------------------------------------------------
# Export/import
------------------------------------------------------------------------------

sudo mkdir -p /mongo/export
sudo chown -R mongodb:mongodb /mongo/export

## Export d'une collection au format CSV

sudo su mongodb -s /bin/bash -c "mongoexport --port 27001 --db test --collection personnes --csv --fields num,nom --out /mongo/export/personnes.csv"

## Export d'une collection au format CSV

sudo su mongodb -s /bin/bash -c "mongoexport --port 27001 --db test --collection personnes --fields num,nom --out /mongo/export/personnes.json"

## Import d'une collection avec remplacement

sudo su mongodb -s /bin/bash -c "mongoimport --port 27001 --db test --collection personnes --drop --file /mongo/export/personnes.json"

------------------------------------------------------------------------------
# Sécurité sur Replicaset
------------------------------------------------------------------------------

## Start node without security

sudo su mongodb -s /bin/bash -c "mongod --port 27001 --dbpath /mongo/rs01/instance01 --logpath /mongo/rs01/instance01/mongod.log --fork"

## Create users on admin DB

mongo --port 27001 admin
> db.createUser( { user: "admin", pwd: "admin1234", roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ] } );
> db.createUser( { user: "root", pwd: "root1234", roles: [ "userAdminAnyDatabase", "readWriteAnyDatabase", "dbAdminAnyDatabase", "clusterAdmin" ] } );

## Create keyfile

sudo su mongodb -s /bin/bash -c "openssl rand -base64 741 > /mongo/rs01/mongodb-keyfile"
sudo chmod 600 /mongo/rs01/mongodb-keyfile

## Stop First node

mongo --port 27001 admin --eval "db.shutdownServer();"

## Start all nodes

sudo su mongodb -s /bin/bash -c "mongod --keyFile /mongo/rs01/mongodb-keyfile --auth --replSet rs01 --shardsvr --port 27001 --dbpath /mongo/rs01/instance01 --logpath /mongo/rs01/instance01/mongod.log --fork"
sudo su mongodb -s /bin/bash -c "mongod --keyFile /mongo/rs01/mongodb-keyfile --auth --replSet rs01 --shardsvr --port 27002 --dbpath /mongo/rs01/instance02 --logpath /mongo/rs01/instance02/mongod.log --fork"
sudo su mongodb -s /bin/bash -c "mongod --keyFile /mongo/rs01/mongodb-keyfile --auth --replSet rs01 --shardsvr --port 27003 --dbpath /mongo/rs01/instance03 --logpath /mongo/rs01/instance03/mongod.log --fork"
sudo su mongodb -s /bin/bash -c "mongod --keyFile /mongo/rs01/mongodb-keyfile --auth  --replSet rs01 --shardsvr --port 27004 --dbpath /mongo/rs01/arbitre --logpath /mongo/rs01/arbitre/mongod.log --fork"

## Initiate Replicaset

mongo --port 27001 admin -p "root1234" -u "root" --authenticationDatabase "admin"
> cfg={_id: "rs01", members:[{_id:1,host:"localhost:27001"}, {_id:2,host:"localhost:27002"}, {_id:3,host:"localhost:27003"}, {_id:4,host:"localhost:27004", arbiterOnly: true}]}
> rs.initiate(cfg);
> db.isMaster();
> rs.status();
> rs.conf();

## Activate nodes

mongo --port 27001 admin -p "root1234" -u "root" --authenticationDatabase "admin" --eval "rs.slaveOk();"
mongo --port 27002 admin -p "root1234" -u "root" --authenticationDatabase "admin" --eval "rs.slaveOk();"
mongo --port 27003 admin -p "root1234" -u "root" --authenticationDatabase "admin" --eval "rs.slaveOk();"

## Stop with authentification

mongo --port 27001 admin -p "root1234" -u "root" --authenticationDatabase "admin" --eval "db.shutdownServer();" 
mongo --port 27001 admin -p "root1234" -u "root" --authenticationDatabase "admin" --eval "db.shutdownServer();" 
mongo --port 27001 admin -p "root1234" -u "root" --authenticationDatabase "admin" --eval "db.shutdownServer();"

------------------------------------------------------------------------------
# map reduce
------------------------------------------------------------------------------

## Jeu de données

use test
db.createCollection('commandes');
db.commandes.insert( { userid: 12345, date: new Date('Apr 28, 2013'), codepost: 13100, articles:[{id:1, nom:'livre', prix:29.90}, {id:2, nom:'eponge', prix:2.90}], totalttc: 120.00, tva: 20.00, totalht: 100.00 } );
db.commandes.insert( { userid: 12345, date: new Date('Apr 28, 2013'), codepost: 13100, articles:[{id:1, nom:'livre', prix:29.90}, {id:2, nom:'eponge', prix:2.90}], totalttc: 120.00, tva: 20.00, totalht: 100.00 } );
db.commandes.insert( { userid: 12345, date: new Date('Apr 28, 2013'), codepost: 13100, articles:[{id:1, nom:'livre', prix:29.90}, {id:2, nom:'eponge', prix:2.90}], totalttc: 120.00, tva: 20.00, totalht: 100.00 } );
db.commandes.insert( { userid: 12345, date: new Date('Apr 28, 2013'), codepost: 13100, articles:[{id:1, nom:'livre', prix:29.90}, {id:2, nom:'eponge', prix:2.90}], totalttc: 120.00, tva: 20.00, totalht: 100.00 } );
db.commandes.insert( { userid: 456789, date: new Date('Apr 29, 2013'), codepost: 13200, articles:[{id:1, nom:'livre', prix:39.90}, {id:2, nom:'eponge', prix:2.90}], totalttc: 130.00, tva: 20.00, totalht: 110.00 } );
db.commandes.insert( { userid: 456789, date: new Date('Apr 29, 2013'), codepost: 13200, articles:[{id:1, nom:'livre', prix:39.90}, {id:2, nom:'eponge', prix:2.90}], totalttc: 130.00, tva: 20.00, totalht: 110.00 } );
db.commandes.insert( { userid: 456789, date: new Date('Apr 29, 2013'), codepost: 13200, articles:[{id:1, nom:'livre', prix:39.90}, {id:2, nom:'eponge', prix:2.90}], totalttc: 130.00, tva: 20.00, totalht: 110.00 } );

## Exemples

------------------

map = function() {emit(this['codepost'], {totalttc: this['totalttc']} ); };

reduce = function(cle, valeur) { var s = {somme:0}; valeur.forEach(function(article){ s.somme += article.totalttc; }); return s; };

db.commandes.mapReduce(map, reduce, {out: 'total_commandes_par_ville'});

db.total_commandes_par_ville.find();

------------------

map = function() {emit(this['codepost'], {totalttc: this['totalttc'], totalht: this['totalht']} ); };

reduce = function(cle, valeur) { var s = {sommettc:0, sommeht:0}; valeur.forEach(function(article){ s.sommettc += article.totalttc; s.sommeht += article.totalht; }); return s; };

db.commandes.mapReduce(map, reduce, {out: 'total_ttc_ht_par_ville'});

db.total_ttc_ht_par_ville.find();

------------------------------------------------------------------------------
# GridFS
------------------------------------------------------------------------------

mongofiles --db ged put ./blazing-saddles.jpg

mongofiles --db ged list

mongofiles --db ged search blazing

mongofiles --db ged get ./blazing-saddles.jpg

mongofiles --db ged delete ./blazing-saddles.jpg