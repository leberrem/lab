angular.module("MailServiceRest", ["ngResource"])
.factory("mailService", function($resource) {

	var URL_API = "http://localhost:8080/api/";

	var resourceMail = $resource(URL_API + "dossiers", null,  {
		"getDossiers": { method: "GET", isArray: true },
		"getDossier": { method: "GET", isArray: false, url: URL_API + "dossiers/:idDossier" },
		"getMail": { method: "GET", isArray: false, url: URL_API + "dossiers/:idDossier/:idMail" },
		"envoiMail": { method: "POST", url: URL_API + "envoi" },
	});

	return {
		getDossiers: function() {
			return resourceMail.getDossiers();
		},
		getDossier: function(valDossier) {
			return resourceMail.getDossier({idDossier: valDossier});
		},
		getMail: function(valDossier, idMail) {
			return resourceMail.getMail({idDossier: valDossier, idMail: idMail});
		},
		envoiMail: function(mail) {
			resourceMail.envoiMail(mail, function(response) {
				alert("Le mail a bien été envoyé !");
			}, function(response) {
				alert("Erreur "+response.status+"dans l'envoi du mail : " + response.data);
			});
		}
	};
})