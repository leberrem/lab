angular.module("MailServiceRest", ["ngResource"])
.factory("mailService", function($resource) {

	var URL_API = "http://localhost:8080/api/";

	var resourceRecupMail = $resource(URL_API + "dossiers/:idDossier/:idMail");
	var resourceEnvoiMail = $resource(URL_API + "envoi");

	return {
		getDossiers: function() {
			return resourceRecupMail.query();
		},
		getDossier: function(valDossier) {
			return resourceRecupMail.get({idDossier: valDossier});
		},
		getMail: function(valDossier, idMail) {
			return resourceRecupMail.get({idDossier: valDossier, idMail: idMail});
		},
		envoiMail: function(mail) {
			resourceEnvoiMail.save(mail, function(response) {
				alert("Le mail a bien été envoyé !");
			}, function(response) {
				alert("Erreur "+response.status+"dans l'envoi du mail : " + response.data);
			});
		}
	};
})