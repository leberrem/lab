angular.module("Webmail", [ "ngSanitize", "MailServiceRest", "MesFiltres", "MesDirectives" ])
.controller("WebmailCtrl", function($scope, $location, $filter, mailService) {

	$scope.dossiers = mailService.getDossiers();

	// -----------------------------------
	// Gestion du tri
	// -----------------------------------
	$scope.champTri = null;
	$scope.triDescendant = false;
	$scope.triEmails = function(champ) {
		if ($scope.champTri == champ) {
			$scope.triDescendant = !$scope.triDescendant;
		} else {
			$scope.champTri = champ;
			$scope.triDescendant = false;
		}
	}

	$scope.cssChevronsTri = function(champ) {
		return {
			'fas fa-angle-down' : $scope.champTri == champ && !$scope.triDescendant,
			'fas fa-angle-up' : $scope.champTri == champ && $scope.triDescendant
		};
	}

	// -----------------------------------
	// Recherche
	// -----------------------------------
	$scope.recherche = null;
	$scope.razRecherche = function() {
		$scope.recherche = null;
	}

	// -----------------------------------
	// création d'emails
	// -----------------------------------

	$scope.afficherNouveauMail = false;

	$scope.envoiMail = function(nouveauMail) {

		mailService.envoiMail(nouveauMail);
		$location.path("/RECEPTION");

	}

	// -----------------------------------
	// Navigation
	// -----------------------------------

	$scope.vueCourante = "vueDossier";
	$scope.dossierCourant = $scope.dossiers[0];
	$scope.emailSelectionne = null;

	$scope.versEmail = function(dossier, email) {
		$location.path("/" + dossier.value + "/" + email.id);
	}

	$scope.selectionEmail = function(valDossier, idEmail) {
		$scope.vueCourante = "vueMail";
		$scope.emailSelectionne = mailService.getMail(valDossier, idEmail);
	};

	$scope.selectionDossier = function(valDossier) {
		$scope.vueCourante = "vueDossier";
		$scope.dossierCourant = mailService.getDossier(valDossier);

		$scope.dossiers.forEach(function(dossier) {
			if (dossier.value == valDossier) {
				dossier.needsUpdate = false;
			}
		});
	};

	$scope.$watch(function() {
		return $location.path();
	}, function(newPath) {
		var tabPath = newPath.split("/");
		if (tabPath.length > 1 && tabPath[1]) {
			var valDossier = tabPath[1];
			switch (valDossier) {
				case ('NEWMAIL') :
					$scope.$broadcast("initFormNouveauMail");
					$scope.vueCourante = "vueNouveauMail";
					$scope.dossierCourant = null;
					break;
				default:
					if (tabPath.length > 2) {
						var idMail = tabPath[2];
						$scope.selectionEmail(valDossier, idMail);
					} else {
						$scope.selectionDossier(valDossier);
					}
			}
		} else {
			$scope.vueCourante = "vueDossier";
			$scope.dossierCourant = $scope.dossiers[0];
		}
	});

	var socket = io("http://localhost:8080");
	socket.on("connect", function(data) {
		console.log("connection avec le serveur réussie");
	});
	socket.on("NEWMAIL", function(idDossier) {
		console.log("nouveau message dans le dossier : " + idDossier);
		if ($scope.dossierCourant && idDossier == $scope.dossierCourant.value) {
			$scope.dossierCourant = mailService.getDossier(idDossier);
			console.log("dossier " + undossier.value + " refresh");
		} else {
			for (var i = 0; i < $scope.dossiers.length; i++) {
				var undossier = $scope.dossiers[i];
				if (undossier.value == idDossier) {
					console.log("dossier " + undossier.value + " needsUpdate");
					undossier.needsUpdate = true;
				}
			}
			$scope.$apply();
		}
	});

});