var http = require('http');
var fs = require('fs');

// npm install mime
var mime = require("mime");

var PORT = 8080;

var envoiFicher = function(res, url) {

    console.log(url + " : request");

    var path = __dirname + "/app" + url
    fs.stat(path, function(err, stats){
        if (!err && stats.isFile()) {
            var flux = fs.createReadStream(path, {
                "flags": "r",
                "autoClose": "true"
            })

            var typeMime = mime.getType(path);

            res.writeHead(200, {"cotent-Type": typeMime});
            flux.pipe(res);
            console.log(url + " : 200 - OK");

        } else {
            envoi404(res);
            console.log(url + " : 404 - not found");
        }
    })

}

var envoi404 = function(res) {
    res.writeHead(404, {"content-Type" : "text/html"});
    res.end("<h1>Page not found!</h1>");
}

http.createServer(function(req, res){

    // Requête
    // console.log("URL : " + req.url);
    // console.log("METHOD : " + req.method);
    // console.log("HEADERS : ");
    // console.log(req.headers);

    switch (req.url) {
        case "/" :
            res.writeHead(301, {"Location": "/index.html"});
            res.end();
            break;
        default :
            envoiFicher(res, req.url);
    }

}).listen(PORT);

console.log("Serveur démarré sur le port " + PORT);