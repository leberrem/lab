var http = require('http');
var fs = require('fs');

var mime = require("mime");
var connect = require("connect");

var PORT = 8080;

var envoiFicher = function(res, url) {

    console.log(url + " : request");

    var path = __dirname + "/app" + url
    fs.stat(path, function(err, stats){
        if (!err && stats.isFile()) {
            var flux = fs.createReadStream(path, {
                "flags": "r",
                "autoClose": "true"
            })

            var typeMime = mime.getType(path);

            res.writeHead(200, {"cotent-Type": typeMime});
            flux.pipe(res);
            console.log(url + " : 200 - OK");

        } else {
            envoi404(res);
            console.log(url + " : 404 - not found");
        }
    })

}

var envoi404 = function(res) {
    res.writeHead(404, {"content-Type" : "text/html"});
    res.end("<h1>Page not found!</h1>");
}

var app = connect();

// MiddleWare 1
app.use(function(req, res, next) {
    if (req.url == "/") {
        res.writeHead(301, {"Location": "/index.html"});
        res.end();
    } else {
        next();
    }
});

// Middleware 2
app.use(function(req, res) {
    envoiFicher(res, req.url);
});

http.createServer(app).listen(PORT);

console.log("Serveur démarré sur le port " + PORT);