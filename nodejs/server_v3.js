var http = require('http');
var fs = require('fs');

var mime = require("mime");
var connect = require("connect");

// Middleware connect
var logger = require("morgan");
var serverStatic = require("serve-static");
var serverFavicon = require("serve-favicon");

var PORT = 8080;

var app = connect();

app.use(logger(":method :url"));
app.use(serverFavicon("app/favicon.ico"));
app.use(serverStatic(__dirname + "/app"));

http.createServer(app).listen(PORT);

console.log("Serveur démarré sur le port " + PORT);