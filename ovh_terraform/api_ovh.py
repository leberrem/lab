#!/usr/bin/python
# -*- encoding: utf-8 -*-
'''
First, install the latest release of Python wrapper: $ pip install ovh
'''
import json
import ovh
from datetime import datetime

DOLLAR_TO_EURO = 0.89 

# Instanciate an OVH Client.
# You can generate new credentials with full access to your account on
# the token creation page
client = ovh.Client(
    endpoint='ovh-eu',               # Endpoint of API OVH Europe (List of available endpoints)
    application_key='tSewb3gyaOLjNeli',    # Application Key
    application_secret='sQFsISTvMVsgKv1zT9XW3s3xHptpb060', # Application Secret
    consumer_key='M39xIQgIMaufd7DA6RZ1IvFBUlSLmPH9',       # Consumer Key
)

print "Welcome", client.get('/me')['firstname']

print "============================================================"

projectsJson = client.get('/cloud/project')
print json.dumps(projectsJson, indent=4)

print "============================================================"

# projects = projectsJson[]
for project in projectsJson:
    print 
    print "project :", project
    print 

    usageJson = client.get('/cloud/project/{}/usage/current'.format(project))
    # print json.dumps(result, indent=4)

    print "  +------------------------"

    totalPriceInstance = 0
    instances = usageJson['hourlyUsage']['instance']
    for instance in instances:
        priceInstance = instance['totalPrice']
        totalPriceInstance += priceInstance
    print "  | Instance =", round(totalPriceInstance * DOLLAR_TO_EURO, 2), "euros"

    totalPriceStorage = 0
    instances = usageJson['hourlyUsage']['storage']
    for instance in instances:
        priceStorage = instance['totalPrice']
        totalPriceStorage += priceStorage
    print "  | Storage =", round(totalPriceStorage * DOLLAR_TO_EURO, 2), "euros"

    totalPrice = totalPriceInstance + totalPriceStorage
    print "  |------------------------"
    print "  | Total =", round(totalPrice * DOLLAR_TO_EURO, 2), "euros"
    print "  +------------------------"

