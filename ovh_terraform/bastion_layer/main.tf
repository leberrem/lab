# Import de la clé SSH au sein d'OpenStack
resource "openstack_compute_keypair_v2" "bastion_keypair" {
  provider = "openstack.ovh"
  name = "bastion_keypair"
  public_key = "${file("~/.ssh/ovh.pub")}"
}

# Création du security group ingress
resource "openstack_networking_secgroup_v2" "secgroup_bastion" {
  name        = "secgroup_bastion"
  description = "security group for bastion"
}

# Rêgles du security group ingress
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_bastion_ingress" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_bastion.id}"
}

# Création d'une instance avec 2 interfaces réseau
resource "openstack_compute_instance_v2" "bastion" {
  provider = "openstack.ovh"
  name = "bastion"
  image_name = "Debian 9"
  flavor_name = "s1-2"
  key_pair = "${openstack_compute_keypair_v2.bastion_keypair.name}"
  security_groups = ["${openstack_networking_secgroup_v2.secgroup_bastion.name}"]
  
   network {
     name = "Ext-Net"
   }

   network {
     name = "${data.terraform_remote_state.network.outputs.private_network_name}"
   }

}
