# -----------------------------------------------------------------------------
# NETWORK OUTPUTS
# -----------------------------------------------------------------------------

output "bastion_public_ip" {
  value = "${openstack_compute_instance_v2.bastion.network.0.fixed_ip_v4}"
}
output "bastion_private_ip" {
  value = "${openstack_compute_instance_v2.bastion.network.1.fixed_ip_v4}"
}


