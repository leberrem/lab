# Configure le fournisseur OpenStack
provider "openstack" {
  auth_url = "https://auth.cloud.ovh.net/v3"
  domain_name = "default"
  alias = "ovh"
  region = "${var.region}"
}

# Configuration du fournisseur OVH
provider "ovh" {
  endpoint = "ovh-eu"
  alias = "ovh"
}
