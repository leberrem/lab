
variable "project_id" {
  description = "The id of the openstack project"
  type = "string"
  default = "73913d0826a745ac8d2f4826491c7260"
}

variable "region" {
  description = "The id of the openstack region"
  type = "string"
  default = "SBG5"
}

data "terraform_remote_state" "network" {
  backend = "swift"
  config = {
    container = "network-layer"
    region_name = "SBG5"
  }
}


