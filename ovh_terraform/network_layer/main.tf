# Création d'un réseau privé
resource "ovh_cloud_network_private" "private_network" {
   provider = "ovh.ovh"
   project_id = "${var.project_id}"
   name = "private_network"
   regions = ["${var.region}"]
}

# Création d'un sous-réseau privé
resource "ovh_cloud_network_private_subnet" "private_subnet" {
   provider = "ovh.ovh"
   project_id = "${var.project_id}"
   region = "${var.region}"
   network_id = "${ovh_cloud_network_private.private_network.id}"
   network = "10.0.1.0/24"
   start = "10.0.1.100"
   end = "10.0.1.200"
   dhcp = true
}
