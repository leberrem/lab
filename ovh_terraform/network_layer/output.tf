# -----------------------------------------------------------------------------
# NETWORK OUTPUTS
# -----------------------------------------------------------------------------

output "private_network_name" {
  value = "${ovh_cloud_network_private.private_network.name}"
}
output "private_network_id" {
  value = "${ovh_cloud_network_private.private_network.id}"
}

output "private_subnet_network_cidr" {
  value = "${ovh_cloud_network_private_subnet.private_subnet.network}"
}

output "private_subnet_network_id" {
  value = "${ovh_cloud_network_private_subnet.private_subnet.id}"
}

