#!/bin/bash

#curl --header "X-Ovh-Application:${OVH_APPLICATION_KEY}" --header "X-Ovh-Application-Secret:${OVH_APPLICATION_SECRET}" --data '{"accessRules":[{"method":"GET","path":"/*"},{"method":"POST","path":"/*"},{"method":"PUT","path":"/*"},{"method":"DELETE","path":"/*"}]}' https://api.ovh.com/1.0/auth/credential

curl -XPOST -H"X-Ovh-Application: ${OVH_APPLICATION_KEY}" -H "Content-type: application/json" \
https://eu.api.ovh.com/1.0/auth/credential  -d '{
    "accessRules": [
        {
            "method": "GET",
            "path": "/*"
        },
        {
            "method": "POST",
            "path": "/*"
        },
        {
            "method": "PUT",
            "path": "/*"
        },
        {
            "method": "DELETE",
            "path": "/*"
        }
    ]
}'